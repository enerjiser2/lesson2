<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Task;
use AppBundle\Entity\Product;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;


class DefaultController extends Controller
{
    public function indexAction()
    {
        $this->redirect('/admin')->send();
    }

    public function newAction(Request $request)
    {
        // create a task and give it some dummy data for this example
        $task = new Task();
        $task->setTask('Write a blog post');
        $task->setDueDate(new \DateTime('tomorrow'));
        $task->setStartDate(new \DateTime('today'));

        $form = $this->createFormBuilder($task)
            ->add('task', TextType::class)
            ->add('startDate', DateType::class)
            ->add('dueDate', DateType::class)
            ->add('save', SubmitType::class, array('label' => 'Create Task'))
            ->getForm();

        return $this->render('default/new.html.twig',
                            [
                            'form' => $form->createView(),
                            ]);
    }

    public function createAction()
    {
        $product = new Product();
        $product->setName('A Foo Bar');
        $product->setPrice('19.99');
        $product->setDescription('Lorem ipsum dolor');

        $em = $this->getDoctrine()->getManager();

        $em->persist($product);
        $em->flush();

        return new Response('Created product id '.$product->getId());
    }

    public function foodAction()
    {
        $html = file_get_html('http://www.foodbay.com');

//        $data = preg_replace('~(\<a.*?([^?">]))~','<a>',$html);
//        $data = preg_replace('~foodbay~i','aaa:',$html);
//        $data = preg_replace('~(\<a.*?.>~','aaa:$1',$html);
//        $data = preg_replace('~(<a.+?\>)~i','aaa:',$html);
//        $data = preg_replace('~(<a.*?)+(?:vk\.com)?(\>)~i','aaa:$1',$html);
//        $data = preg_replace('~(<a.*)()(.\>)~i','aaa:---$2----',$html);
        //$data = preg_replace('~(<a.*?)(?!foodbay*?)(\>)~i','LINK:<<<$1>>>><<$1>>',$html);
        //$data = preg_replace('~(?!<a.*foodbay*?)(<a.*?)(\>)~i','LINK:<<<$1>>>><<$2>>',$html);
        //$data = preg_replace('~((<a)(.!*foodbay*?))(\>)~i','LINK:$1',$html);
        $data = preg_replace('~(<a.*?\>)(?!(<a.*?vk\.com*?\>))~i','[aaa]:$2',$html);

        return new Response($data);

        exit;
    }

}

